<#
CitiTrends Active Directory Tool Kit
written by Naomi Sinsheimer
Updated 12/16/2020
#>

$c = Get-Credential #Elevate Prompt
$citidomain = "192.168.31.238"
$ctstores = "192.168.3.9"

<# Menu Function #>
function Show-Menu 
{
    param([string]$Title = 'CitiTrends AD Toolkit')

    Clear-Host
    Write-Host "================ $Title ================"
    Write-Host ""
    Write-Host "1: Account Lookup"
    Write-Host "2: Password Reset"
    Write-Host "3: Account Unlock"
    Write-Host "4: Extend Password 90 Days"
    Write-Host "5: Leave Disable"
    Write-Host "6: Termination Disable"
    Write-Host "Q: Quit"
    Write-Host ""
}

<# Domain Check Function #>
function CitiDomainCheck ([string]$user)
{
    if((Get-ADUser -Server $citidomain -Filter {sAMAccountName -eq $user }) -eq $Null) #If input user is NOT in CitiDomain
    {
        if((Get-ADUser -Server $ctstores -Filter {sAMAccountName -eq $user }) -eq $Null) #If input user is NOT in CitiDomain or CTstores
        {
            #Doesn't exist
            return 'null'
        }
        else
        {
            #Ctstores
            Write-Host "User is a member of CTstores."
            return '192.168.3.9'
        }
    }
    else
    {
        #Citidomain
        Write-Host "User is a member of Citidomain"
        return '192.168.31.238'
    }
}

<# Password Reset Function #>
function pwReset ([String]$domain,[String]$user,[string]$pwd)
{
    $confirm = Read-Host "Do you wish to proceed? Y/N"
    confirmInput $confirm
    Set-ADAccountPassword -server $domain -Credential $c -Identity $user -Reset -NewPassword (ConvertTo-SecureString -AsPlainText $pwd -Force)
    Write-Host "Password changed successfully."
}

<# Domain Unlock Function #>
function unlock ([string]$domain,[string]$user)
{
    Unlock-ADAccount -server $domain -Credential $c -Identity $user
    Write-Host "Account unlocked successfully"
}

<# Account Information Function #>
function lookUp ([string]$domain,[string]$user)
{
    get-aduser -server $domain $user -properties "UserPrincipalName ", "msDS-UserPasswordExpiryTimeComputed", 
         "Title", "Created","Department", "Enabled", "CanonicalName", "PasswordExpired", "LockedOut", "AccountLockoutTIme", 
         "LogonCount", "LastBadPasswordAttempt", "EmailAddress" | Select-Object -property Name, EmailAddress, UserPrincipalName , 
         Title, Department, Created, Enabled, PasswordExpired, LockedOut, AccountLockoutTime, 
         CanonicalName, logonCount, LastBadPasswordAttempt, @{Name="PwdExpiryDate";Expression={[datetime]::FromFileTime($_."msDS-UserPasswordExpiryTimeComputed")}}  | Format-List
}

<# Verify Function#>
<# Verifies that a user is a part of either Citidomain or CTstores #>
function verifyUser([string]$domain)
{
    if($domain -eq 'null')
            {
                Write-Host "Sorry, no such user exists. Returning to main menu."
                Wait
                break
            }
}

function confirmInput([string]$confirmation)
{
    if($confirmation -eq "y")
    {
        return $confirmation
    }
    elseif($confirmation -eq "n")
    {
        Write-Host "No changes applied. Returning to main menu."
        Wait
        break
    }
}

<# Disable User #>
function disableUser ([string]$domain,[string]$user)
{
    $confirm = Read-Host "Do you wish to proceed? Y/N"
    confirmInput $confirm
    Disable-ADAccount -Credential $c -Server $domain -Identity $user
    "User is disabled. Returning to main menu."
}

<# Move User to OU #>
function moveUser ([string]$server,[string]$OU,[string]$DC,[string]$user)
{
    Get-ADUser $user -Server $server | Move-ADObject -Credential $c -Server $server -TargetPath "OU=$OU,DC=$DC,DC=cititrends,DC=com"
    Write-Host "User relocated to $OU."
}

<# Wait Function #>
function Wait
{
    Start-Sleep -Seconds 1.7
}

<# Code Execute#>
do
{

    Show-Menu
    $selection = Read-Host -prompt "Please make a selection"

    switch ($selection)
    {
        <# Account Lookup, credit Adam Pulsney for help #>
        '1'
        {
            Clear-Host
            Write-Host "=============== CitiTrends Domain Account Look Up. ==============="
            Write-Host ""
            $user = Read-Host "Enter User"

            $domain = CitiDomainCheck $user
            verifyUser $domain
            lookUp $domain $user
            
            Write-Host -NoNewLine 'Press any key to return to main menu...';
            $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

        }
        <# Password Reset Option#>
        '2'
        { 
            Clear-Host
            Write-Host "=============== CitiTrends Domain Password Reset ==============="
            Write-Host ""
            $user = Read-Host "Enter user" #Prompt user whose password is being changed

            $domain = CitiDomainCheck $user
            verifyUser $domain

            Write-Host ""
            $pwd = Read-Host "Enter new password" #New password
            pwReset $domain $user $pwd
            unlock $domain $user
            Wait
        }
        <# Domain Unlock Option #>
        '3'
        {
            Clear-Host
            Write-Host "=============== CitiTrends Domain Account Unlock ==============="
            Write-Host ""
            $user = Read-Host "Enter User"

            $domain = CitiDomainCheck $user
            VerifyUser $domain
            unlock $domain $user
            Wait
        }
        <# Extend Password #>
        '4'
        {
            Clear-Host
            Write-Host "=============== Extend Password 90 Days ==============="
            Write-Host ""
            $user = Read-Host "Enter User"

            $domain = CitiDomainCheck $user
            VerifyUser $domain

            Set-ADUser -server $domain -Credential $c -Identity $user -Replace @{pwdLastSet=0}
            Set-ADUser -server $domain -Credential $c -Identity $user -Replace @{pwdLastSet=-1}
            Set-ADUser -server $domain -Credential $c -Identity $user -ChangePasswordAtLogon:$false
            unlock $domain $user
            Wait
        }
        <# Leave Disable #>
        '5'
        {
            Clear-Host
            Write-Host "=============== FMLA Disable ==============="
            Write-Host ""
            $user = Read-Host "Enter User"

            $domain = CitiDomainCheck $user
            VerifyUser $domain
            lookUp $domain $user

            disableUser $domain $user
            $leavesOU = "Leaves"

            if($domain -eq $citidomain) <# Citidomain Leaves #>
            {
                moveUser $domain $leavesOU "citidomain" $user
            }
            else <# CTstores Leaves #>
            {
                moveUser $domain $leavesOU "ctstores" $user
            }
        Wait
        Wait
        }
        <# Termination Disable #>
        '6'
        {
            Clear-Host
            Write-Host "=============== Termination Disable ==============="
            Write-Host ""
            $user = Read-Host "Enter User"

            $domain = CitiDomainCheck $user
            VerifyUser $domain

            disableUser $domain $user
            $disabledOU = "DisabledUsers"

            if($domain -eq $citidomain) <# Citidomain #>
            {
                moveUser $domain $disabledOU "citidomain" $user
            }
            else <# CTstores #>
            {
                moveUser $domain $disabledOU "ctstores" $user
            }
        }
    }
}
until ($selection -eq 'q')
