<#
CitiTrends Active Directory Tool Kit
written by Naomi Sinsheimer
Updated 3/18/2021
#>

$c = Get-Credential #Elevate Prompt
$citidomain = "192.168.31.238"
$ctstores = "192.168.3.9"

<# Menu Function #>
function Show-Menu 
{
    param([string]$Title = 'CitiTrends AD Toolkit')

    Clear-Host
    Write-Host "================ $Title ================"
    Write-Host ""
    Write-Host "1: Account Lookup"
    Write-Host "2: Password Reset"
    Write-Host "3: Account Unlock"
    Write-Host "4: Extend Password 90 Days"
    Write-Host "5: Leave Disable"
    Write-Host "6: Leave Enable (Not tested in production as of 3/18/21)"
    Write-Host "7: Termination Disable"
    Write-Host "Q: Quit"
    Write-Host ""
}

<# Domain Check Function #>
function CitiDomainCheck ([string]$user)
{
    if((Get-ADUser -Server $citidomain -Filter {sAMAccountName -eq $user }) -eq $Null) #If input user is NOT in CitiDomain
    {
        if((Get-ADUser -Server $ctstores -Filter {sAMAccountName -eq $user }) -eq $Null) #If input user is NOT in CitiDomain or CTstores
        {
            #Doesn't exist
            return 'null'
        }
        else
        {
            #Ctstores
            Write-Host "$user is a member of CTstores."
            return '192.168.3.9'
        }
    }
    else
    {
        #Citidomain
        Write-Host "$user is a member of Citidomain"
        return '192.168.31.238'
    }
}

<# Password Reset Function #>
function pwReset ([String]$domain,[String]$user,[string]$pwd)
{
    confirmInput
    Set-ADAccountPassword -server $domain -Credential $c -Identity $user -Reset -NewPassword (ConvertTo-SecureString -AsPlainText $pwd -Force)
    Write-Host "Password for $user changed successfully."
}

<# Domain Unlock Function #>
function unlock ([string]$domain,[string]$user)
{
    Unlock-ADAccount -server $domain -Credential $c -Identity $user
    Write-Host "Account $user unlocked successfully"
}

<# Account Information Function #>
function lookUp ([string]$domain,[string]$user)
{
    get-aduser -server $domain $user -properties "UserPrincipalName ", "msDS-UserPasswordExpiryTimeComputed", 
         "Title", "Created","Department", "Enabled", "CanonicalName", "PasswordExpired", "LockedOut", "AccountLockoutTIme", 
         "LogonCount", "LastBadPasswordAttempt", "EmailAddress" | Select-Object -property Name, EmailAddress, UserPrincipalName , 
         Title, Department, Created, Enabled, PasswordExpired, LockedOut, AccountLockoutTime, 
         CanonicalName, logonCount, LastBadPasswordAttempt, @{Name="PwdExpiryDate";Expression={[datetime]::FromFileTime($_."msDS-UserPasswordExpiryTimeComputed")}}  | Format-List
}

<# Verify Function#>
<# Verifies that a user is a part of either Citidomain or CTstores #>
function verifyUser([string]$domain)
{
    if($domain -eq 'null')
            {
                Write-Host "Sorry, no such user exists."
                mainMenu
                break
            }
}

function confirmInput([string]$confirmation)
{
    $confirmation = Read-Host "Do you wish to proceed? Y/N"
    if($confirmation -eq "y")
    {
        return
    }
    elseif($confirmation -eq "n")
    {
        Write-Host "No changes applied."
        mainMenu
        break
    }
    else
    {
        Write-host "No valid input."
        mainMenu
        break
    }
}

<# Disable User #>
function disableUser ([string]$domain,[string]$user)
{
    confirmInput
    Disable-ADAccount -Credential $c -Server $domain -Identity $user
    "User $user is disabled."
}

function enableUser ([string]$domain,[string]$user)
{
    confirmInput
    Enable-ADAccount -Credential $c -Server $domain -Identity $user
    "User $user is enabled."
}

<# Move User to OU #>
function moveUser ([string]$server,[string]$OU,[string]$DC,[string]$user)
{
    Get-ADUser $user -Server $server | Move-ADObject -Credential $c -Server $server -TargetPath "OU=$OU,DC=$DC,DC=cititrends,DC=com"
    Write-Host "User relocated to $OU."
}

function mainMenu
{
    Write-Host -NoNewLine 'Press any key to return to main menu...';
    [void][System.Console]::ReadKey($true)
}

<# Code Execute#>
do
{

    Show-Menu
    $selection = Read-Host -prompt "Please make a selection"

    switch ($selection)
    {
        <# Account Lookup, credit Adam Pulsney for help #>
        '1'
        {
            Clear-Host
            Write-Host "=============== CitiTrends Domain Account Look Up. ==============="
            Write-Host ""
            $user = Read-Host "Enter User"

            $domain = CitiDomainCheck $user
            verifyUser $domain
            lookUp $domain $user
            
            mainMenu

        }
        <# Password Reset Option#>
        '2'
        { 
            Clear-Host
            Write-Host "=============== CitiTrends Domain Password Reset ==============="
            Write-Host ""
            $user = Read-Host "Enter user" #Prompt user whose password is being changed

            $domain = CitiDomainCheck $user
            verifyUser $domain

            Write-Host ""
            $pwd = Read-Host "Enter new password" #New password
            pwReset $domain $user $pwd
            unlock $domain $user
            mainMenu
        }
        <# Domain Unlock Option #>
        '3'
        {
            Clear-Host
            Write-Host "=============== CitiTrends Domain Account Unlock ==============="
            Write-Host ""
            $user = Read-Host "Enter User"

            $domain = CitiDomainCheck $user
            VerifyUser $domain
            unlock $domain $user
            mainMenu
        }
        <# Extend Password #>
        '4'
        {
            Clear-Host
            Write-Host "=============== Extend Password 90 Days ==============="
            Write-Host ""
            $user = Read-Host "Enter User"

            $domain = CitiDomainCheck $user
            VerifyUser $domain

            Set-ADUser -server $domain -Credential $c -Identity $user -Replace @{pwdLastSet=0}
            Set-ADUser -server $domain -Credential $c -Identity $user -Replace @{pwdLastSet=-1}
            Set-ADUser -server $domain -Credential $c -Identity $user -ChangePasswordAtLogon:$false
            Write-Host "$user password has been extended for 90 days."
            unlock $domain $user
            mainMenu
        }
        <# Leave Disable #>
        '5'
        {
            Clear-Host
            Write-Host "=============== Leave Disable ==============="
            Write-Host ""
            $user = Read-Host "Enter User"

            $domain = CitiDomainCheck $user
            VerifyUser $domain
            lookUp $domain $user

            disableUser $domain $user
            $leavesOU = "Leaves"

            if($domain -eq $citidomain) <# Citidomain Leaves #>
            {
                moveUser $domain $leavesOU "citidomain" $user
            }
            else <# CTstores Leaves #>
            {
                moveUser $domain $leavesOU "ctstores" $user
            }
        mainMenu
        }
        '6'
        <# Leave Re-Enable #>
        {
            Clear-Host
            Write-Host "=============== Leave Enable ==============="
            Write-Host ""
            $user = Read-Host "Enter User"

            $domain = CitiDomainCheck $user
            VerifyUser $domain
            lookUp $domain $user

            enableUser $domain $user

            while($true)
            {
                Write-Host "1. Coleman"
                Write-Host "2. New York"
                Write-Host "3. Darlington"
                Write-Host "4. Roland"
                Write-Host "5. CTstores"
                Write-Host "Q. Cancel"
                $ou = Read-Host "Please select an OU to move the user to."

                $leavearray = @("1", "2", "3", "4", "5", "6", "Q")

                if($ou -in $leavearray)
                {
                    break
                }
                else 
                {
                    Write-Host "Invalid selection, please try again."
                    continue    
                }
            }

            <# Switch to choose OU #>
            $destinationDomain = "citidomain"
            switch($ou)
            {
                <# Coleman #>
                '1'
                {
                    $destinationOU = "Users"
                }
                <# New York #>
                '2'
                {
                    $destinationOU = "NewYork"
                }
                <# Darlington #>
                '3'
                {
                    $destinationOU = "Darlington"
                }
                <# Roland #>
                '4'
                {
                    $destinationOU = "Roland"
                }
                <# CTStores #>
                '5'
                {
                    $destinationOU = "Users"
                    $destinationDomain = "ctstores"
                }
            }
            Write-Host "Moving $user to $destinationOU in $destinationDomain."
            confirmInput
            moveUser $domain $destinationOU $destinationDomain $user
            Write-Host "$user enabled and relocated to $destinationOU in $destinationDomain."
        }
        <# Termination Disable #>
        '7'
        {
            Clear-Host
            Write-Host "=============== Termination Disable ==============="
            Write-Host ""
            $user = Read-Host "Enter User"

            $domain = CitiDomainCheck $user
            VerifyUser $domain

            disableUser $domain $user
            $disabledOU = "DisabledUsers"

            if($domain -eq $citidomain) <# Citidomain #>
            {
                moveUser $domain $disabledOU "citidomain" $user
            }
            else <# CTstores #>
            {
                moveUser $domain $disabledOU "ctstores" $user
            }
        }
    }
}
until ($selection -eq 'q')
